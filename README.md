# Nurse Rostering Problem instances #

This repository contains the data for the paper *Solving a Real-World Nurse
Rostering Problem by Simulated Annealing* by Sara Ceschia, Luca Di
Gaspero, Vincenzo Mazzaracchio, Giuseppe Policante, Andrea
Schaerf, Operations Research for Health Care, 36, 100379 (2023).

### Dataset ###

The dataset collects 34 real world instances coming from 10 wards of
different healthcare institutions located in the North Italy.

Instances are available in the folder [`Dataset`](Dataset) in *JSON* format.

### Solutions ###

Best solutions are available in the folder [`Solutions`](Solutions) in
*JSON* format.
